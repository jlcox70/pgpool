PACKAGE := pgpool
TAG := $(shell git tag |grep $(PACKAGE) |sed "s/$(PACKAGE)-//g" |head -n 1 )
NAME = registry.gitlab.com/jlcox70/$(PACKAGE)

PATCH := $(shell echo ${TAG} |cut -d- -f1 |cut -d. -f3 )
PAST_TAG := $(shell echo ${TAG} |cut -d- -f2)
# NEW_PATCH := $(shell echo $$(( $(PATCH) + 1 )) )
BASE := $(shell echo ${TAG} |cut -d- -f1 |cut -d. -f1,2 |sed 's/^v//g' )
REVCOUNT := $(shell git rev-list  `git rev-list --tags --no-walk --max-count=1`..HEAD --count)
ifeq ($(git rev-list `git rev-list --tags --no-walk --max-count=1`..HEAD --count),1)
	NEW_PATCH := $(shell echo $$(( $(PATCH) + 1 )) )
else
	NEW_PATCH := $(shell echo $$(( $(PATCH) + $(REVCOUNT) )) )
endif


# ifeq ($(shell git tag -l --points-at HEAD |wc -l) , 0)
        VERSION := $(shell echo "${BASE}.${NEW_PATCH}" )
        OLD_VERSION := $(shell echo "${BASE}.${PATCH}" )




build:
	docker build -t $(NAME):$(VERSION) .

build-nocache:
	docker build -t $(NAME):$(VERSION) --no-cache .

test:
	env NAME=$(NAME) VERSION=$(VERSION) bats test/test.bats

tag-latest:
	docker tag $(NAME):$(VERSION) $(NAME):latest

push:
	docker push $(NAME):$(VERSION)

push-latest:
	docker push $(NAME):latest

release: build tag-latest push push-latest

git-tag-version: release
	git tag -a v$(VERSION) -m "v$(VERSION)"
	git push origin v$(VERSION)
	git push --tags
