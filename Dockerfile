FROM centos:8

RUN yum install -y https://www.pgpool.net/yum/rpms/4.1/redhat/rhel-8-x86_64/pgpool-II-release-4.1-1.noarch.rpm &&\
    yum install pgpool-II-pg12 -y; mkdir /var/log/pgpool/ &&\
    yum clean all &&\
    \rm -fr /var/log/* &&\
    mkdir /var/log/pgpool
COPY pgpool.conf /etc/pgpool-II/
COPY healthz /usr/local/bin

CMD [ "pgpool", "-f","/etc/pgpool-II/pgpool.conf", "-n" ]